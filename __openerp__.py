# -*- coding: utf-8 -*-

{
     'name': "Wedding Planner",
    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    'description':"To retrieve data from fb lead add",
    'author': "Py Solutions",
    'website': "http://www.pysolutions.com",
    'category': 'Tools',
    'version': '0.1',
    'depends': ['base'],
    'data': [
	    'wedding_planner_view.xml',
    ],
    'demo': [],
    'installable': True
}
