# -*- coding: utf-8 -*-

from openerp import fields, models, api


class Banquet(models.Model):
    _name = 'banquet.book'
    
    name = fields.Char()
    
    #registration .....................
    
class Wedding_reg(models.Model):
     _name = 'wedding.registration' 
    
     
     party_name = fields.Char()
     wedding_budget = fields.Float()
     wedding_datetime = fields.Datetime()
     religion = fields.Selection([
                   ('hindu','Hindu'),
                   ('muslim','Muslim'),
                   ('punjabi','Punjabi'),
                   ('christian','Christian'),
                   ('gujrati','Gujrati'),
                   ('bengali','Bengali')])    
                   
     @api.multi
     def register(self):
        print self.party_name
        print self.wedding_budget
        print self.wedding_datetime
        print self.religion       
        res_partner_obj = self.env['res.partner']
        res_partner_obj.create({'name': self.party_name})
    
    



class Wedding(models.Model):
    _name = 'wedding.details'
    _rec_name = 'wedding_date'
    
    bride_name = fields.Char('Name of the Bride')
    groom_name = fields.Char('Name of the Groom')
    religion = fields.Selection([
                   ('hindu','Hindu'),
                   ('muslim','Muslim'),
                   ('punjabi','Punjabi'),
                   ('christian','Christian'),
                   ('gujrati','Gujrati'),
                   ('bengali','Bengali')])
    wedding_budget = fields.Float()
    wedding_date = fields.Date()
    banquet = fields.Many2one('banquet.book','Banquet Booked')
    events = fields.One2many('event.details','wedding_associated_events','Events')
    attendees = fields.Many2one('res.partner')
    attendees_wedding = fields.Many2many('res.partner','attendees_wedding_info','wedding','attendess')

    @api.multi
    def a(self):
        print "sachin"   

    @api.multi
    def ab(self):
         print "sky"
         print self.bride_name
         print self.groom_name
         print self.religion
         

class Event(models.Model):
    _name = 'event.details' 
    
    event_location = fields.Char()
    event_datetime = fields.Datetime()
    event_type = fields.Char()
    religion = fields.Selection([
                   ('hindu','Hindu'),
                   ('muslim','Muslim'),
                   ('punjabi','Punjabi'),
                   ('christian','Christian'),
                   ('gujrati','Gujrati'),
                       ('bengali','Bengali')]) 
    wedding_associated_events = fields.Many2one('wedding.details')
                       
                       
                       
class Event_type(models.Model):
    _name = 'event.type' 
    
    event_name = fields.Char()
    religion = fields.Selection([
                   ('hindu','Hindu'),
                   ('muslim','Muslim'),
                   ('punjabi','Punjabi'),
                   ('christian','Christian'),
                   ('gujrati','Gujrati'),
                   ('bengali','Bengali')])
